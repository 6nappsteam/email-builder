6nApps Canvas Template
======================

Intégration de Canvas template dans symfony 4.

Branch `master` of this bundle requires at least __PHP 7.1__ and __Symfony 4.2__ components

Installation
============

en console
+ composer require sixnapps/canvas-template-bundle
+ bin/console assets:install --symlink

modifier le fichier config/packages/twig.yaml
```
twig:
    default_path: '%kernel.project_dir%/templates'
    debug: '%kernel.debug%'
    strict_variables: '%kernel.debug%'
    form_themes: ['templates/canvas/form_override.html.twig']
```
