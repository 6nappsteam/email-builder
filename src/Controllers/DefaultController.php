<?php
	
	namespace Sixnapps\EmailBuilderBundle\Controllers;
	
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	
	/**
	 * Class DefaultController
	 *
	 * @package Sixnapps\EmailBuilderBundle\Controllers
	 */
	class DefaultController extends AbstractController
	{
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function index()
		{
			return $this->render('@SixnappsEmailBuilder/index.html.twig');
		}
	}
